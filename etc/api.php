<?php

use AlexGaj\api\Controllers\TaskController;
use AlexGaj\lib\Components\DbConnectionManager;
use AlexGaj\lib\Components\Request;
use AlexGaj\lib\Components\SimpleRender;
use AlexGaj\lib\Components\SimpleRouter;

return [
    'components' => [
        'connectionManager' => [
            'class' => DbConnectionManager::class,
            'connections' => [
                'main' => [
                    'username' => 'root',
                    'password' => 'root',
                    'dbname' => 'todo_list',
                    'host' => 'db',
                    'port' => 3306
                ]
            ]
        ],

        'router' => [
            'class' => \AlexGaj\lib\Components\Route\SimpleRouter::class,
            'routes' => [
                [
                    'url' => '/api/tasks/:task_id',
                    'controller' => TaskController::class,
                    'action' => 'getTaskAction',
                    'method' => 'get'
                ],
                [
                    'url' => '/api/tasks',
                    'controller' => TaskController::class,
                    'action' => 'getAllTasksAction',
                    'method' => 'get'
                ],
               [
                    'url' => '/api/tasks',
                    'controller' => TaskController::class,
                    'action' => 'getAllTasksAction',
                    'method' => 'get'
                ],
                [
                    'url' => '/api/tasks',
                    'controller' => TaskController::class,
                    'action' => 'createTaskAction',
                    'method' => 'post'
                ],
                [
                    'url' => '/api/tasks/:task_id',
                    'controller' => TaskController::class,
                    'action' => 'deleteTaskAction',
                    'method' => 'delete'
                ],
                [
                    'url' => '/api/tasks/:task_id',
                    'controller' => TaskController::class,
                    'action' => 'updateTaskAction',
                    'method' => 'put'
                ],
            ]
        ],
        'request' => [
            'class' => Request::class
        ],
        'render' => [
            'class' => SimpleRender::class
        ]
    ],
];