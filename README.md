# DEPLOY
buld container
``./undevelop.sh build``

run containers
``./undevelop.sh up``

stop containers
``./undevelop.sh down``

launch tests
``./undevelop.sh runtest``

# ENDPOINTS
Add new task
  ``curl --location --request POST 'localhost/api/tasks' --form 'task=Test' --form 'due_to=2009-06-06' --form 'priority=3'``
    
Get task
``curl --location --request GET 'localhost/api/tasks/1'``

Get all tasks
``curl --location --request GET 'localhost/api/tasks'``

Delete Task
``curl --location --request DELETE 'localhost/api/tasks/1'``

Update task
``curl --location --request PUT 'localhost/api/tasks/1' --data-raw 'status=2&task=New task&priority=1&due_to=07/07/2020'``

