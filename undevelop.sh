#!/usr/bin/env bash

    if [ "$1" == "build" ]; then
    	cp .env.example .env

       	docker-compose build

		docker-compose up -d

        docker-compose exec app composer install

        docker-compose run --rm app bash -c "mysql -h db -P 3306 -uroot -proot -D todo_list < /var/www/data/migrations/init.sql"

    elif [ "$1" == "down" ]; then
    	docker-compose down

    elif [ "$1" == "up" ]; then
        	docker-compose up -d
   elif [ "$1" == "runtest" ]; then
    	docker-compose run --rm app ./vendor/bin/phpunit ./src/Tests
	fi