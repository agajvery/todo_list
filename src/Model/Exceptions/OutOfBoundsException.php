<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/3/20
 * Time: 5:08 PM
 */

namespace AlexGaj\Model\Exceptions;


use RuntimeException;

class OutOfBoundsException extends RuntimeException
{

}