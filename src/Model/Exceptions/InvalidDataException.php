<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 11:32 AM
 */

namespace AlexGaj\Model\Exceptions;


use RuntimeException;

class InvalidDataException extends RuntimeException
{

}