<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 11:21 PM
 */

namespace AlexGaj\Model\Exceptions;


use RuntimeException;

class StoreDataException extends RuntimeException
{

}