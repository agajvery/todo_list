<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 10:52 PM
 */

namespace AlexGaj\Model\Repository;


use AlexGaj\Model\Entity\Task as TaskEntity;

interface TaskRepositoryInterface
{
    public function getById(int $taskId): ?TaskEntity;

    public function save(TaskEntity $entity): TaskEntity;

    public function getAll(): array;

    public function delete(int $taskId): bool;
}