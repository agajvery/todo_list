<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 10:50 PM
 */

namespace AlexGaj\Model\Repository;


use AlexGaj\Model\Entity\Task as TaskEntity;
use AlexGaj\Model\Exceptions\StoreDataException;
use AlexGaj\lib\Persistence\PersistenceInterface;

class TaskRepository implements TaskRepositoryInterface
{
    /**
     * @var
     */
    private $persistence;

    public function __construct(PersistenceInterface $persistence)
    {
        $this->persistence = $persistence;
    }

    public function save(TaskEntity $entity): TaskEntity
    {
        if ($entity->getId() !== null && $this->persistence->getById($entity->getId())) {
            if ($this->persistence->update($entity->getId(), $this->prepareDataForSave($entity))) {
                return $entity;
            }
        } else {
            $taskId = $this->persistence->add($this->prepareDataForSave($entity));
            if ($taskId !== null) {
                $entity->setId($taskId);
                return $entity;
            }
        }

        throw new StoreDataException("Some problem store data");
    }

    public function getAll(): array
    {
        $data = $this->persistence->getAll();
        return $this->collect($data);
    }

    public function delete(int $taskId): bool
    {
       return $this->persistence->delete($taskId);
    }

    public function getById(int $taskId): ?TaskEntity
    {
        $data = $this->persistence->getById($taskId);
        if ($data !== null) {
            return $this->createEntity($data);
        }

        return null;
    }

    protected function collect(array $data): array
    {
        $entities = [];

        foreach ($data as $row) {
            $entities[] = $this->createEntity($row);
        }

        return $entities;
    }

    protected function prepareDataForSave(TaskEntity $entity): array
    {
        return [
            'task' => $entity->getTask(),
            'task_status' => $entity->getStatus(),
            'priority' => $entity->getPriority(),
            'due_to_ts' => $entity->getDueDateTimeStamp()
        ];
    }

    protected function createEntity($params): TaskEntity
    {
        $entity = new TaskEntity();

        $entity->setId($params['id']);
        $entity->setStatus($params['task_status']);
        $entity->setPriority($params['priority']);
        $entity->setDueDateTimeStamp($params['due_to_ts']);
        $entity->setTask($params['task']);

        return $entity;
    }
}