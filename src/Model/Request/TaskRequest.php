<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 12:31 AM
 */

namespace AlexGaj\Model\Request;


class TaskRequest
{
    public $task;

    public $dueToDate;

    public $status;

    public $priority;

    public $id;
}