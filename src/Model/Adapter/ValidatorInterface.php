<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/3/20
 * Time: 7:29 AM
 */

namespace AlexGaj\Model\Adapter;


interface ValidatorInterface
{
    public function validate(array $data, array $rules): bool;

    public function getErrors(): array;

    public function setCustomMessage(array $messages);
}