<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/3/20
 * Time: 7:31 AM
 */

namespace AlexGaj\Model\Adapter;

use SimpleValidator\Validator;

class SimpleValidator implements ValidatorInterface
{
    private $errors = [];

    private $customMessages = [];

    public function setCustomMessage(array $messages)
    {
        $this->customMessages = $messages;
    }

    public function validate(array $data, array $rules): bool
    {
        $this->errors = [];

        $validateResult = Validator::validate($data, $rules);
        if ($validateResult->isSuccess()) {
            return true;
        } else {

            $validateResult->customErrors($this->customMessages);
            $this->errors = $validateResult->getErrors();

            return false;
        }
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}