<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 12:31 AM
 */

namespace AlexGaj\Model\Entity;


use AlexGaj\Model\Exceptions\InvalidDataException;
use JsonSerializable;

class Task extends BaseEntity implements JsonSerializable
{
    const STATUS_FINISHED = 1;

    const STATUS_IN_PROGRESS = 2;

    const STATUS_OPENED = 3;

    const PRIORITY_LOW = 1;

    const PRIORITY_MEDIUM = 2;

    const PRIORITY_HIGH = 3;

    const STATUSES = [
        self::STATUS_FINISHED,
        self::STATUS_IN_PROGRESS,
        self::STATUS_OPENED
    ];

    const PRIORITIES = [
        self::PRIORITY_LOW,
        self::PRIORITY_MEDIUM,
        self::PRIORITY_HIGH
    ];

    protected $task;

    protected $dueDateTimeStamp;

    protected $status;

    protected $priority;


    /**
     * @return mixed
     */
    public function getTask(): string
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     */
    public function setTask(string $task): void
    {
        $this->task = $task;
    }

    /**
     * @return mixed
     */
    public function getDueDateTimeStamp(): int
    {
        return $this->dueDateTimeStamp;
    }

    /**
     * @param mixed $dueDateTimeStamp
     */
    public function setDueDateTimeStamp(int $dueDateTimeStamp): void
    {
        $this->dueDateTimeStamp = $dueDateTimeStamp;
    }

    /**
     * @return mixed
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus(int $status): void
    {
        if (in_array($status, self::STATUSES)) {
            $this->status = $status;
        } else {
            throw new InvalidDataException("Invalid type of status.");
        }
    }

    /**
     * @return mixed
     */
    public function getPriority():int
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority(int $priority): void
    {
        if (in_array($priority, self::PRIORITIES)) {
            $this->priority = $priority;
        } else {
            throw new InvalidDataException("Invalid type of priority");
        }
    }

    public function getDueToDate()
    {
        return date('m/d/Y', $this->dueDateTimeStamp);
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'task' => $this->task,
            'dut_to_date' => date('m/d/Y', $this->dueDateTimeStamp),
            'status' => $this->status,
            'priority' => $this->priority
        ];
    }
}