<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 8:58 AM
 */

namespace AlexGaj\Model\Entity;


abstract class BaseEntity
{
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function __set($name, $value)
    {
        $methodName = 'set' . str_replace('_', '', ucwords($name, '_'));
        call_user_func([$this, $methodName], $value);
    }
}