<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 11:03 AM
 */

namespace AlexGaj\Model\Factory;

use AlexGaj\lib\Components\ConnectionManagerInterface;
use AlexGaj\lib\Persistence\TaskMysqlStorage;
use AlexGaj\Model\Adapter\SimpleValidator;
use AlexGaj\Model\Repository\TaskRepository;
use AlexGaj\Model\Service\TaskService;

class TaskServiceFactory
{
    public static function create(ConnectionManagerInterface $connectionManager): TaskService
    {
        $repositoryStorage = new TaskMysqlStorage($connectionManager->getConnection('main'));
        $repository = new TaskRepository($repositoryStorage);

        return new TaskService($repository, new SimpleValidator());
    }
}