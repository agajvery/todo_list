<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 12:31 AM
 */

namespace AlexGaj\Model\Service;


use AlexGaj\Model\Adapter\ValidatorInterface;
use AlexGaj\Model\Entity\Task as TaskEntity;
use AlexGaj\Model\Exceptions\InvalidDataException;
use AlexGaj\Model\Exceptions\OutOfBoundsException;
use AlexGaj\Model\Repository\TaskRepositoryInterface;
use AlexGaj\Model\Request\TaskRequest;

class TaskService
{
    const RULE_SCOPE_CREATE = 'create';

    const RULE_SCOPE_UPDATE = 'update';

    /**
     * @var RepositoryTaskInterface
     */
    private $repository;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(TaskRepositoryInterface $repository, ValidatorInterface $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;

        $this->initValidator();
    }

    public function getById(int $taskId): TaskEntity
    {
        $entity = $this->repository->getById($taskId);
        if ($entity === null) {
            throw new OutOfBoundsException("Task doesn't exist");
        }

        return $entity;
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function create(TaskRequest $request)
    {
        $validateData = [
            'task' => $request->task,
            'due_to' => $request->dueToDate,
            'priority' => $request->priority
        ];

        if ($this->validate($validateData, self::RULE_SCOPE_CREATE)) {
            $entity = new TaskEntity();

            $entity->setTask($request->task);
            $entity->setPriority($request->priority);
            $entity->setDueDateTimeStamp(strtotime($request->dueToDate));
            $entity->setStatus(TaskEntity::STATUS_OPENED);

            return $this->repository->save($entity);
        } else {
            throw new InvalidDataException(implode('; ', $this->getValidationErrors()));
        }
    }

    public function delete(int $taskId): bool
    {
        $entity = $this->repository->getById($taskId);
        if ($entity) {
            return $this->repository->delete($taskId);
        } else {
            throw new OutOfBoundsException("Task doesn't exist");
        }
    }

    public function update(TaskRequest $request)
    {
        $validateData = [
            'id' => $request->id,
            'status' => $request->status,
            'priority' => $request->priority,
            'due_to' => $request->dueToDate,
            'task' => $request->task
        ];

        if ($this->validate($validateData, self::RULE_SCOPE_UPDATE)) {

            $entity = new TaskEntity();

            $entity->setId($request->id);
            $entity->setStatus($request->status);
            $entity->setPriority($request->priority);
            $entity->setTask($request->task);
            $entity->setDueDateTimeStamp(strtotime($request->dueToDate));

            if ($this->repository->getById($entity->getId()) === null) {
                throw new OutOfBoundsException("Task doesn't exist");
            }

            return $this->repository->save($entity);
        } else {
            $errors = $this->getValidationErrors();
            throw new InvalidDataException(implode('; ', $errors));
        }
    }

    private function initValidator()
    {
        $this->validator->setCustomMessage([
            'is_valid_date' => 'Invalid date format',
            'valid_value_priority' => 'Invalid type of priority',
            'is_exist_row' => "Task doesn't exist",
            'valid_value_status' => "Invalid type of status"
        ]);
    }

    private function validate(array $data, string $scope)
    {
        return $this->validator->validate(
            $data,
            $this->getValidationRules($scope)
        );
    }

    private function getValidationRules(string $scope): array
    {

        $rules = [
            self::RULE_SCOPE_CREATE => [
                'task' => [
                    'required',
                    'max_length(255)'
                ],
                'due_to' => [
                    'required',
                    'is_valid_date' => function($input) {
                         return $input > 0 ? true : false;
                    }
                ],
                'priority' => [
                    'required',
                    'integer',
                    'valid_value_priority' => function($input) {
                        return in_array($input, TaskEntity::PRIORITIES);
                    }
                ]
            ],
            self::RULE_SCOPE_UPDATE => [
                'id' => [
                    'required',
                    'integer',
                ],
                'status' => [
                    'required',
                    'valid_value_status' => function($input) {
                        return in_array($input, TaskEntity::STATUSES);
                    }
                ],
                'task' => [
                    'required',
                    'max_length(255)'
                ],
                'due_to' => [
                    'required',
                    'is_valid_date' => function($input) {
                        return $input > 0 ? true : false;
                    }
                ],
                'priority' => [
                    'integer',
                    'required',
                    'valid_value_priority' => function($input) {
                        return in_array($input, TaskEntity::PRIORITIES);
                    }
                ]
            ]
        ];

        return $rules[$scope];
    }

    private function getValidationErrors(): array
    {
        return $this->validator->getErrors();
    }
}