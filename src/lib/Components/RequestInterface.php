<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/1/20
 * Time: 8:21 PM
 */

namespace AlexGaj\lib\Components;


interface RequestInterface
{
    public function getPath(): string;

    public function getUri(): string;

    public function getMethod(): string;

    public function get(string $paramName, $default = null);

    public function post(string $paramName, $default = null);

    public function allPost();

    public function allGet();

    public function addGetParams(array $params);
}