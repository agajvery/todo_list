<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/1/20
 * Time: 7:58 PM
 */

namespace AlexGaj\lib\Components;

class Request implements RequestInterface, ComponentInterface
{
    private $get = [];

    private $post = [];

    public function getUri(): string
    {
        return $_SERVER['REQUEST_URI'];
    }

    public function getPath(): string
    {
        $uri = $this->getUri();
        if (preg_match('/(.*)\?/', $uri, $match)) {
            $path = $match[1];
        } else {
            $path = $uri;
        }

        return $path;
    }

    /**
     * return type of request (get|post|put|delete|head...)
     *
     * @return string
     */
    public function getMethod(): string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    /**
     *
     *
     * @param string $paramName
     * @param null $default
     * @return null
     */
    public function get(string $paramName, $default = null)
    {
        return $this->get[$paramName] ?? $default;
    }

    /**
     *
     * @param string $paramName
     * @param null $default
     * @return null
     */
    public function post(string $paramName, $default = null)
    {
        return $this->post[$paramName] ?? $default;
    }

    public function allGet()
    {
        return $this->get;
    }

    public function allPost()
    {
        return $this->post;
    }

    public function init()
    {
        switch ($this->getMethod()) {
            case 'put':
                $postVars = [];
                parse_str(file_get_contents("php://input"),$postVars);
                $this->post = $postVars;
                break;

            case 'post':
                $this->post = $_POST;
                break;

            default:
                $this->get = $_GET;
        }
    }

    public function addGetParams(array $params)
    {
        $this->get = array_merge($this->get, $params);
    }
}