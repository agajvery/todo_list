<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 6/29/20
 * Time: 4:57 PM
 */

namespace AlexGaj\lib\Components;


class SimpleRender implements RenderViewInterface, ComponentInterface
{
    private $viewPath;


    public function setViewPath(string $viewPath)
    {
        $this->viewPath = $viewPath;
    }

    public function view(string $viewName, array $params = [], int $code = 200)
    {
        ob_start();
        include $this->viewPath . $viewName . '.php';
        $html = ob_get_clean();

        return $this->render($html, $code);
    }

    public function json(array $payload, int $code = 200)
    {
        header('Content-Type: application/json');
        return $this->render(json_encode($payload), $code);
    }

    private function render(string $content, int $code = 200)
    {
        http_response_code($code);
        echo $content;
    }

    public function init()
    {
        // TODO: Implement init() method.
    }
}