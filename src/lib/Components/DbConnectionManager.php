<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 11:34 PM
 */

namespace AlexGaj\lib\Components;


use AlexGaj\lib\Exceptions\InvalidConfigurationException;
use PDO;

class DbConnectionManager implements ComponentInterface, ConnectionManagerInterface
{
    protected $connections = [];

    protected $cached = [];

    public function setConnections(array $connections)
    {
        $this->connections = $connections;
    }

    public function getConnection(string $connectionName): PDO
    {
        if (isset($this->connections[$connectionName])) {
            if (!isset($this->cached[$connectionName])) {
                $connection = $this->connections[$connectionName];

                $dsn = "mysql:dbname={$connection['dbname']};host={$connection['host']};port={$connection['port']}";
                $pdo = new PDO($dsn, $connection['username'], $connection['password']);

                $this->cached[$connectionName] = $pdo;
            }
        } else {
            throw new InvalidConfigurationException("Connection {$connectionName} doesn't exists");
        }

        return $this->cached[$connectionName];
    }

    public function init()
    {
        return true;
    }
}