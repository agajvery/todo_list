<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 6/29/20
 * Time: 4:49 PM
 */

namespace AlexGaj\lib\Components;


interface ComponentInterface
{
    public function init();
}