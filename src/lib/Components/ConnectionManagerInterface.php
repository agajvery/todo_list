<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 6/29/20
 * Time: 5:01 PM
 */

namespace AlexGaj\lib\Components;


use PDO;

interface ConnectionManagerInterface
{
    public function getConnection(string $connectionName): PDO;
}