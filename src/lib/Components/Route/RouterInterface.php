<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/1/20
 * Time: 7:51 PM
 */

namespace AlexGaj\lib\Components\Route;

use AlexGaj\lib\Components\RequestInterface;


interface RouterInterface
{
    public function getController(RequestInterface $request): ?RouteRuleInterface;
}