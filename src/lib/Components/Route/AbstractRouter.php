<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/5/20
 * Time: 6:44 AM
 */

namespace AlexGaj\lib\Components\Route;


use AlexGaj\lib\Components\ComponentInterface;
use AlexGaj\lib\Components\RequestInterface;


abstract class AbstractRouter implements ComponentInterface, RouterInterface
{
    /**
     * @var RouteRuleInterface[]
     */
    protected $routes = [];

    abstract protected function createRouteRule(string $rule, string $method, string $controller, string $action): RouteRuleInterface;

    public function setRoutes(array $routes)
    {
        foreach ($routes as $params) {
            if (!isset($params['controller']) || !isset($params['action']) || !isset($params['url'])) {
                throw new InvalidConfigurationException('Controller and action are required');
            }

            if (!isset($params['method'])) {
                $params['method'] = 'get';
            }

            $this->routes[] = $this->createRouteRule(
                $params['url'],
                $params['method'],
                $params['controller'],
                $params['action']
            );
        }
    }

    public function getController(RequestInterface $request): ?RouteRuleInterface
    {

        $path = $request->getPath();
        $method = $request->getMethod();

        foreach ($this->routes as $route) {

            if ($route->isValidRule($path, $method)) {
                return $route;
            }

        }

        return null;
    }

    public function init()
    {
        // TODO: Implement init() method.
    }
}