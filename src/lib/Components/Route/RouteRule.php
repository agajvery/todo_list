<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/5/20
 * Time: 6:09 AM
 */

namespace AlexGaj\lib\Components\Route;


class RouteRule implements RouteRuleInterface
{
    private $rule;

    private $method;

    private $controller;

    private $action;

    private $regExp;

    private $urlParams = [];

    public function __construct($rule, $method, $controller, $action)
    {
        $this->rule = $rule;
        $this->method = $method;

        $this->controller = $controller;
        $this->action = $action;

        $this->regExp = $this->generateRegExp($this->rule);

    }

    public function isValidRule(string $path, string $method): bool
    {
        if (preg_match($this->regExp['test'], $path, $matches) && $method == $this->method) {
            foreach ($this->regExp['params'] as $index => $key) {
                $this->urlParams[$key] = $matches[$index];
            }
            return true;
        }

        return false;
    }

    private function generateRegExp($rule): array
    {
        $params = [];
        $regExpChunks = [];
        $paramPosition = 1;

        $urlChunks = explode('/', $rule);

        foreach ($urlChunks as $chunk) {
            if (mb_strpos($chunk, ':') !== false) {
                $regExpChunks[] = '(.*)';
                $params[$paramPosition++] = substr($chunk, 1);
            } else {
                $regExpChunks[] = $chunk;
            }
        }

        return [
            'test' => '/^' . implode('\/', $regExpChunks) . '$/',
            'params' => $params
        ];
    }

    public function getUrlParams(): array
    {
        return $this->urlParams;
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function getAction(): string
    {
        return $this->action;
    }
}