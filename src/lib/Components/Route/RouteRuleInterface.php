<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/5/20
 * Time: 6:48 AM
 */

namespace AlexGaj\lib\Components\Route;


interface RouteRuleInterface
{
    public function isValidRule(string $path, string $method): bool;

    public function getUrlParams(): array;

    public function getController(): string;

    public function getAction(): string;

}