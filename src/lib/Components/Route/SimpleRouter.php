<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/1/20
 * Time: 7:48 PM
 */

namespace AlexGaj\lib\Components\Route;

class SimpleRouter extends AbstractRouter
{

    protected function createRouteRule(string $rule, string $method, string $controller, string $action): RouteRuleInterface
    {
        return new RouteRule($rule, $method, $controller, $action);
    }
}