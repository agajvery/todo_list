<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 6/29/20
 * Time: 3:05 PM
 */

namespace AlexGaj\lib\Components;


interface RenderViewInterface
{
    public function view(string $viewName, array $params = [], int $code = 200);

    public function json(array $payload, int $code = 200);
}