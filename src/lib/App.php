<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 6/29/20
 * Time: 2:44 PM
 */

namespace AlexGaj\lib;

use AlexGaj\lib\Components\ServiceLocator;
use AlexGaj\lib\Exceptions\InvalidConfigurationException;

require_once FRAMEWORK_PATH . '/helpers.php';

class App
{
    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var ServiceLocator
     */
    public static $components;

    public function __construct(array $configs)
    {
        $this->configure($configs);
    }

    public function run()
    {
        $route = self::$components->router->getController(self::$components->request);
        if ($route === null) {
            header('HTTP/1.0 404 Not Found');
            exit(0);
        }

        self::$components->request->addGetParams($route->getUrlParams());

        $controllerClassName = $route->getController();
        $actionName = $route->getAction();

        $controller = new $controllerClassName();
        call_user_func_array([$controller, $actionName], [self::$components->request]);
    }

    private function configure(array $configs)
    {
        foreach ($configs as $paramName => $value) {
            $methodName = 'set' . ucfirst($paramName);
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            } else {
                throw new InvalidConfigurationException(sprintf('Undefined config param %s', $paramName) );
            }
        }
    }

    private function setComponents(array $components)
    {
        self::$components = ServiceLocator::create($components);
    }
}