<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 6/29/20
 * Time: 2:46 PM
 */

use AlexGaj\lib\Components\RenderViewInterface;
use AlexGaj\lib\Components\ConnectionManagerInterface;
use AlexGaj\lib\App;
use AlexGaj\lib\Components\RequestInterface;


function render(): RenderViewInterface
{
    return App::$components->render;
}

function dbConnection(): ConnectionManagerInterface
{
    return App::$components->dbConnection;
}

function request(): RequestInterface
{
    return App::$components->request;
}