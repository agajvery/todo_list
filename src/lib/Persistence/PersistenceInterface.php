<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 10:37 PM
 */

namespace AlexGaj\lib\Persistence;


interface PersistenceInterface
{
    public function update(int $id, array $data): bool;

    public function add(array $data): ?int;

    public function getById(int $id): ?array;

    public function delete(int $id): bool;

    public function getAll(): array;
}