<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 11:49 PM
 */

namespace AlexGaj\lib\Persistence;


class TaskMysqlStorage extends Mysql
{

    public function getTableName(): string
    {
        return 'tasks';
    }

    public function getPrimaryKey(): string
    {
        return 'id';
    }
}