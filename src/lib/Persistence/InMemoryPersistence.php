<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/3/20
 * Time: 5:32 AM
 */

namespace AlexGaj\lib\Persistence;


class InMemoryPersistence implements PersistenceInterface
{
    private $data = [];

    private $lastId = 0;

    public function update(int $id, array $data): bool
    {
        if (isset($this->data[$id])) {
            $data['id'] = $id;
            $this->data[$id] = $data;
            return true;
        }

        return false;
    }

    public function add(array $data): ?int
    {
        $nextId = ++$this->lastId;

        $data['id'] = $nextId;
        $this->data[$nextId] = $data;

        return $nextId;
    }

    public function getById(int $id): ?array
    {
        if (isset($this->data[$id])) {
            return $this->data[$id];
        }

        return null;
    }

    public function delete(int $id): bool
    {
        if (!isset($this->data[$id])) {
            return false;
        }

        unset($this->data[$id]);
        return true;
    }

    public function getAll(): array
    {
        return $this->data;
    }
}