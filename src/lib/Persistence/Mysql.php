<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 10:37 PM
 */

namespace AlexGaj\lib\Persistence;


use PDO;

abstract class Mysql implements PersistenceInterface
{
    private $pdo;

    public abstract function getTableName(): string;

    public abstract function getPrimaryKey(): string;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getAll(): array
    {
        $query = 'SELECT * FROM ' . $this->getTableName();
        $rows = $this->getRows($query);

        return $rows;
    }

    public function getById(int $id): ?array
    {
        $query = 'SELECT * FROM ' . $this->getTableName() . ' WHERE ' . $this->getPrimaryKey() . ' = :id';
        $statement = $this->pdo->prepare($query);
        $statement->execute([':id' => $id]);

        $row = $statement->fetch(PDO::FETCH_ASSOC);
        return is_array($row) ? $row : null;
    }


    public function delete(int $id): bool
    {
        $query = (
            'DELETE FROM ' . $this->getTableName() .
            ' WHERE ' . $this->getPrimaryKey() . ' = :id'
        );

        $statement = $this->pdo->prepare($query);
        return $statement->execute([':id' => $id]);
    }

    public function update(int $id, array $params): bool
    {
        $bindParams = [':id' => $id];
        $updateParams = [];

        foreach (array_keys($params) as $field) {
            $key = ':' . $field;
            $updateParams[] = ($field . ' = ' . $key);
            $bindParams[$key] = $params[$field];
        }

        $query = ('
            UPDATE ' . $this->getTableName() . ' 
            SET ' . implode(', ', $updateParams) . '
            WHERE ' . $this->getPrimaryKey() . ' = :id'
        );

        $statement = $this->pdo->prepare($query);
        return $statement->execute($bindParams);

    }

    public function add(array $params): ?int
    {
        $insertParams = [];
        $bindParams = [];

        $columns = array_keys($params);
        foreach ($columns as $column) {
            $key = ':' . $column;
            $insertParams[] = $key;
            $bindParams[$key] = $params[$column];
        }

        $query = ('
            INSERT INTO ' . $this->getTableName() .
            '(' . implode(', ', $columns) . ') '.
            'VALUES (' . implode(', ', $insertParams) . ')'
        );

//        echo $query;

        $statement = $this->pdo->prepare($query);

//        echo var_dump($statement->errorInfo(),$bindParams);
        if ($statement->execute($bindParams)) {
            return $this->pdo->lastInsertId();
        }
        return null;
    }

    protected function getRows(string $sqlQuery, array $params = []): array
    {
        $statement = $this->pdo->prepare($sqlQuery);
        $statement->execute($params);
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);

        return is_array($rows) ? $rows : [];
    }
}