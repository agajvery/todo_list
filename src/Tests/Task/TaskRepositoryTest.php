<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/3/20
 * Time: 5:52 AM
 */

namespace AlexGaj\Tests\Task;


use AlexGaj\lib\Persistence\InMemoryPersistence;
use AlexGaj\Model\Entity\Task;
use AlexGaj\Model\Repository\TaskRepository;
use PHPUnit\Framework\TestCase;

class TaskRepositoryTest extends TestCase
{
    /**
     * @var TaskRepository
     */
    private $repository;

    protected function setUp(): void
    {
        $this->repository = new TaskRepository(new InMemoryPersistence());
    }

    /**
     * @param $task
     * @param $dueToTimeStamp
     * @param $taskStatus
     * @param $priority
     *
     * @dataProvider additionalProvider
     */
    public function testCanAddData($task, $dueToTimeStamp, $taskStatus, $priority)
    {
        $entity = new Task();

        $entity->setTask($task);
        $entity->setDueDateTimeStamp($dueToTimeStamp);
        $entity->setPriority($priority);
        $entity->setStatus($taskStatus);

        $taskId = $this->repository->save($entity)->getId();

        $this->assertGreaterThan(0, $taskId);

        $retrieveEntity = $this->repository->getById($taskId);

        $this->assertNotNull($retrieveEntity);

        $this->assertEquals($task, $retrieveEntity->getTask());
        $this->assertEquals($taskStatus, $retrieveEntity->getStatus());
        $this->assertEquals($priority, $retrieveEntity->getPriority());
        $this->assertEquals($dueToTimeStamp, $retrieveEntity->getDueDateTimeStamp());

    }

    public function testCanUpdateStatus()
    {
        $initStatus = Task::STATUS_OPENED;
        $updateStatus = Task::STATUS_FINISHED;

        $entity = new Task();

        $entity->setTask('New task');
        $entity->setDueDateTimeStamp(time());
        $entity->setPriority(Task::PRIORITY_LOW);
        $entity->setStatus($initStatus);

        $storeEntity = $this->repository->save($entity);
        $storeEntity->setStatus($updateStatus);

        $this->repository->save($storeEntity);

        $this->assertEquals($updateStatus, $this->repository->getById($storeEntity->getId())->getStatus());
    }

    public function testCanDeleteItem()
    {
        $firstEntity = new Task();

        $firstEntity->setTask('New task');
        $firstEntity->setDueDateTimeStamp(time());
        $firstEntity->setPriority(Task::PRIORITY_LOW);
        $firstEntity->setStatus(Task::STATUS_OPENED);

        $firstStoreEntity = $this->repository->save($firstEntity);

        $secondEntity = new Task();

        $secondEntity->setTask('New task 2');
        $secondEntity->setDueDateTimeStamp(time());
        $secondEntity->setPriority(Task::PRIORITY_LOW);
        $secondEntity->setStatus(Task::STATUS_OPENED);

        $secondStoreEntity = $this->repository->save($secondEntity);

        $this->repository->delete($firstStoreEntity->getId());

        $this->assertNull($this->repository->getById($firstStoreEntity->getId()));
        $this->assertNotNull($this->repository->getById($secondStoreEntity->getId()));
    }

    public function additionalProvider()
    {
        return [
            ['New Task 1', time(), Task::STATUS_OPENED, Task::PRIORITY_HIGH],
            ['New Task 2', time(), Task::STATUS_OPENED, Task::PRIORITY_LOW],
            ['New Task 3', time(), Task::STATUS_OPENED, Task::PRIORITY_MEDIUM],
            ['New Task 4', time(), Task::STATUS_FINISHED, Task::PRIORITY_HIGH],
            ['New Task 5', time(), Task::STATUS_IN_PROGRESS, Task::PRIORITY_HIGH],
        ];
    }

}