<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/2/20
 * Time: 9:42 AM
 */

namespace AlexGaj\Tests\Task;


use AlexGaj\Model\Entity\Task;
use PHPUnit\Framework\TestCase;
use AlexGaj\Model\Exceptions\InvalidDataException;

class TaskEntityTest extends TestCase
{
    public function testSetId()
    {
        $id = 1;

        $entity = new Task();
        $entity->setId($id);

        $this->assertEquals($id, $entity->getId());
    }

    public function testSetStatus()
    {
        $status = Task::STATUS_IN_PROGRESS;

        $entity = new Task();
        $entity->setStatus($status);

        $this->assertEquals($status, $entity->getStatus());

    }

    public function testSetInvalidStatus()
    {
        $this->expectException(InvalidDataException::class);

        $entity = new Task();
        $entity->setStatus(100);
    }

    public function testSetInvalidPriority()
    {
        $this->expectException(InvalidDataException::class);

        $entity = new Task();
        $entity->setPriority(100);
    }
}