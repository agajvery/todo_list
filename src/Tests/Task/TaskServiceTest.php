<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/3/20
 * Time: 5:25 AM
 */

namespace AlexGaj\Tests\Task;

use AlexGaj\lib\Persistence\InMemoryPersistence;
use AlexGaj\Model\Adapter\SimpleValidator;
use AlexGaj\Model\Exceptions\InvalidDataException;
use AlexGaj\Model\Repository\TaskRepository;
use AlexGaj\Model\Request\TaskRequest;
use AlexGaj\Model\Service\TaskService;
use AlexGaj\Model\Entity\Task as TaskEntity;
use PHPUnit\Framework\TestCase;

class TaskServiceTest extends TestCase
{
    /**
     * @var TaskService
     */
    private $taskService;

    protected function setUp(): void
    {
        $repository = new TaskRepository(new InMemoryPersistence());
        $this->taskService = new TaskService($repository, new SimpleValidator());
    }

    /**
     * @dataProvider additionalProvider
     */
    public function testCanPersistNewTask($task, $dueToDate, $priotity)
    {
        $taskRequest = new TaskRequest();

        $taskRequest->task = $task;
        $taskRequest->dueToDate = $dueToDate;
        $taskRequest->priority = $priotity;

        $taskId = $this->taskService->create($taskRequest)->getId();

        $entity = $this->taskService->getById($taskId);

        $this->assertEquals($task, $entity->getTask());
        $this->assertEquals($dueToDate, $entity->getDueToDate());
        $this->assertEquals($priotity, $entity->getPriority());
    }

    public function testFailSaveInvalidStatus()
    {
        $this->expectException(InvalidDataException::class);

        $taskRequest = new TaskRequest();

        $taskRequest->task = 'New task 1';
        $taskRequest->dueToDate = date('m/d/Y', time());
        $taskRequest->priority = TaskEntity::PRIORITY_LOW;
        $taskRequest->status = 100;
        $taskRequest->id = 1;

        $this->taskService->update($taskRequest);

    }

    public function additionalProvider()
    {
        return [
            ['New Task 1', date('m/d/Y', time() + rand(3600, 86400)), TaskEntity::PRIORITY_HIGH],
            ['New Task 2', date('m/d/Y', time() + rand(3600, 86400)), TaskEntity::PRIORITY_LOW],
            ['New Task 3', date('m/d/Y', time() + rand(3600, 86400)), TaskEntity::PRIORITY_MEDIUM],
            ['New Task 4', date('m/d/Y', time() + rand(3600, 86400)), TaskEntity::PRIORITY_HIGH],
            ['New Task 5', date('m/d/Y', time() + rand(3600, 86400)), TaskEntity::PRIORITY_HIGH],
        ];
    }


}