<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/1/20
 * Time: 11:09 PM
 */

namespace AlexGaj\api\Controllers;


abstract class BaseController
{
    const API_ERROR_INVALID_DATA = 1000;

    const API_ERROR_SOME_PROBLEM = 1001;

    const API_ERROR_ITEM_NOT_FIND = 1002;

    public function success(array $payload = [])
    {
        $response = [
            'success' => true,
            'payload' => $payload
        ];
        return render()->json($response, 200);
    }

    public function error(int $errorCode, string $message, int $code = 200)
    {
        $response = [
            'success' => false,
            'error' => [
                'code' => $errorCode,
                'message' => $message
            ]
        ];

        return render()->json($response, $code);
    }
}