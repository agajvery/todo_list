<?php
/**
 * Created by PhpStorm.
 * User: alex.gajvoronskij
 * Date: 7/1/20
 * Time: 7:32 PM
 */

namespace AlexGaj\api\Controllers;

use AlexGaj\lib\App;
use AlexGaj\lib\Components\RequestInterface;
use AlexGaj\Model\Exceptions\InvalidDataException;
use AlexGaj\Model\Exceptions\OutOfBoundsException;
use AlexGaj\Model\Factory\TaskServiceFactory;
use AlexGaj\Model\Request\TaskRequest;

class TaskController extends BaseController
{
    private $taskService = null;

    public function __construct()
    {
        $this->taskService = TaskServiceFactory::create(App::$components->connectionManager);
    }

    public function getTaskAction(RequestInterface $httpRequest)
    {
        $taskId = $httpRequest->get('task_id');

        try {
            $taskEntity = $this->taskService->getById($taskId);
            return $this->success(['task' => $taskEntity]);
        } catch(OutOfBoundsException $exception) {
            return $this->error(self::API_ERROR_ITEM_NOT_FIND, $exception->getMessage(), 404);
        }
    }

    public function getAllTasksAction(RequestInterface $httpRequest)
    {
        $tasks = $this->taskService->getAll();
        return $this->success($tasks);
    }

    public function createTaskAction(RequestInterface $httpRequest)
    {
        $taskRequest = new TaskRequest();

        $taskRequest->task = $httpRequest->post('task');
        $taskRequest->dueToDate = $httpRequest->post('due_to');
        $taskRequest->priority = $httpRequest->post('priority');

        try {
            $taskId = $this->taskService->create($taskRequest)->getId();
            return $this->success(['task_id' => $taskId]);
        } catch (InvalidDataException $exception) {
            return $this->error(self::API_ERROR_INVALID_DATA, $exception->getMessage());
        }
    }

    public function deleteTaskAction(RequestInterface $httpRequest)
    {
        $taskId = (int) $httpRequest->get('task_id');
        if ($taskId > 0) {
            try {
                if ($this->taskService->delete($taskId)) {
                    return $this->success();
                } else {
                    return $this->error(self::API_ERROR_SOME_PROBLEM, 'Some problem when try delete task');
                }
            } catch (OutOfBoundsException $exception) {
                return $this->error(self::API_ERROR_ITEM_NOT_FIND, $exception->getMessage(), 404);
            }
        } else {
            return $this->error(self::API_ERROR_INVALID_DATA, 'task_is is required');
        }
    }

    public function updateTaskAction(RequestInterface $httpRequest)
    {
        $request = new TaskRequest();

        $request->task = $httpRequest->post('task');
        $request->dueToDate = $httpRequest->post('due_to');
        $request->priority = $httpRequest->post('priority');
        $request->id = $httpRequest->get('task_id');
        $request->status = $httpRequest->post('status');

        try {
            $entity = $this->taskService->update($request);
            return $this->success(['item' => $entity]);
        } catch (InvalidDataException $exception) {
            return $this->error(self::API_ERROR_INVALID_DATA, $exception->getMessage());
        } catch (OutOfBoundsException $exception) {
            return $this->error(self::API_ERROR_ITEM_NOT_FIND, $exception->getMessage(), 404);
        }
    }
}