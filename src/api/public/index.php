<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit', '52M');

use AlexGaj\lib\App;

ini_set('display_errors', true);

$basePath = realpath(__DIR__ . '/../../../');

define('BASE_PATH', $basePath);
define('FRAMEWORK_PATH', $basePath . '/src/lib');

require_once BASE_PATH . '/vendor/autoload.php';
$config = require_once  BASE_PATH . '/etc/api.php';

$app = new App($config);
$app->run();